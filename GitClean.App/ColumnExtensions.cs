namespace GitClean.App
{
    public static class ColumnExtensions
    {
        public static string Pad(this string texto, int width)
        {
            if (texto.Length > width)
                return texto.Substring(0, width - 1) + '+';

            return texto.PadRight(width, ' ');
        }

        public static string Pad(this bool valor, int width)
        {
            return valor.ToString().Pad(width);
        }        
    }
}