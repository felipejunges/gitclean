using LibGit2Sharp;
using System.Collections.Generic;
using System.Linq;

namespace GitClean.App
{
    public class BranchService
    {
        public static IEnumerable<Branch> ObterBranchsExcluir(Repository repository)
        {
            var branchsLocais = repository.Branches.Where(o => !o.IsRemote).ToList();
            var branchsRemotas = repository.Branches.Where(o => o.IsRemote).ToList();

            return branchsLocais
                .Where(o => branchsRemotas
                    .Where(w => w.FriendlyName == o.TrackedBranch?.FriendlyName)
                    .Count() == 0
                );
        }
    }
}