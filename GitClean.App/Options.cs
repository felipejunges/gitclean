using CommandLine;

namespace GitClean.App
{
    public class Options
    {
        [Option('p', "prune", Required = false, HelpText = "Faz 'fetch --all --prune' antes.")]
        public bool FetchPruneFrist { get; set; }

        [Option('d', "dir", Required = false, HelpText = "Diretório do repositório.")]
        public string Diretorio { get; set; }
        
        [Option('r', Required = false, HelpText = "Remove branchs locais excluídas.")]
        public bool RemoverBranchsLocaisExcluidas { get;set;}
    }
}