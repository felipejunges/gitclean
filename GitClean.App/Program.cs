﻿using CommandLine;
using LibGit2Sharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace GitClean.App
{
    class Program
    {
        static void Main(string[] args)
        {
            Parser.Default.ParseArguments<Options>(args)
                   .WithParsed<Options>(o =>
                   {
                       if (o.FetchPruneFrist)
                       {
                       }

                       if (string.IsNullOrEmpty(o.Diretorio))
                           o.Diretorio = ".";

                       if (!Directory.Exists(o.Diretorio))
                           throw new DirectoryNotFoundException($"Diretório {o.Diretorio} não existe.");

                       try
                       {
                           using (var repository = new Repository(o.Diretorio))
                           {
                               ImprimirBranches(repository.Branches.ToList(), repository, false);

                               ImprimirBranches(BranchService.ObterBranchsExcluir(repository).ToList(), repository, o.RemoverBranchsLocaisExcluidas);
                           }
                       }
                       catch (RepositoryNotFoundException)
                       {
                           Console.WriteLine($"O diretório '{o.Diretorio}' não contém um repositório de Git.");
                       }
                       catch (Exception ex)
                       {
                           Console.WriteLine($"Erro obtendo dados do repositório Git no diretório '{o.Diretorio}': {ex.Message}.");
                       }
                   });
        }

        private static void ImprimirBranches(List<Branch> branches, Repository repository, bool removerBranchs)
        {
            if (branches.Count == 0)
                return;

            Console.WriteLine("");
            Console.WriteLine($"{"FriendlyName".Pad(50)} {"IsRemote".Pad(15)} {"TrackedBranch?.FriendlyName".Pad(50)}");
            Console.WriteLine($"{"".PadLeft(50, '-')} {"".PadLeft(15, '-')} {"".PadLeft(50, '-')}");

            foreach (var branch in branches)
            {
                Console.Write($"{branch.FriendlyName.Pad(50)} {branch.IsRemote.Pad(15)} {(branch.TrackedBranch != null ? branch.TrackedBranch.FriendlyName : string.Empty).Pad(50)}");

                if (removerBranchs)
                {
                    try
                    {
                        repository.Branches.Remove(branch);
                        Console.Write(" - Removendo: Sucesso!");
                    }
                    catch (Exception ex)
                    {
                        Console.Write($" - Removendo: Falha: {ex.Message}");
                    }
                }

                Console.WriteLine();
            }
        }
    }
}